package com.hcl.matrimony.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.RegisterDto;
import com.hcl.matrimony.service.UserService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

@RestController
@RequestMapping("/users")
public class UserController {
	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	@PostMapping("/register")
	public ResponseEntity<ApiResponse> register(@Valid @RequestBody RegisterDto registerDto){
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.register(registerDto));
	}
	
	@PutMapping("/login")
	public ResponseEntity<ApiResponse> login(@Valid @Email(message = "invalid email")
	@NotBlank@RequestParam String email,@RequestParam String password)
	{
		return ResponseEntity.status(HttpStatus.OK).body(userService.login(email,password));
	}
	
	@PutMapping("/logout/{email}")
	public ResponseEntity<ApiResponse> logout(@Valid @Email(message = "invalid email")@NotBlank@PathVariable String email)
	{
		return ResponseEntity.status(HttpStatus.OK).body(userService.logout(email));
	}
	

}
