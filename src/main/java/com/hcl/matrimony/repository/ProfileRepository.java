package com.hcl.matrimony.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.matrimony.entity.Profile;
import java.util.List;


public interface ProfileRepository extends JpaRepository<Profile, Long>{
	Profile findByEmail(String email);
List<Profile> findByGenderIgnoreCaseAndAgeBetweenAndMaritalStatusIgnoreCaseAndReligionIgnoreCaseAndCasteIgnoreCase(String gender,int age1,int age2,String maritalStatus,String religion,String caste);

}
