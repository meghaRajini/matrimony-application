package com.hcl.matrimony.dto;

import com.hcl.matrimony.entity.Gender;
import com.hcl.matrimony.entity.MaritalStatus;
import com.hcl.matrimony.entity.Religion;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProfileDto {
private String firstName;
private String lastName;
private Gender gender;
private Religion religion;
private MaritalStatus maritalStatus;
private String caste;
private String email;
private int age;
private double income;
private String zodiacSign;
private String profession;
private AddressDto address;
}
