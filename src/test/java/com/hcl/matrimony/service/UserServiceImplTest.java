package com.hcl.matrimony.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.matrimony.dto.ApiResponse;
import com.hcl.matrimony.dto.RegisterDto;
import com.hcl.matrimony.entity.User;
import com.hcl.matrimony.exception.ResourceConflictExists;
import com.hcl.matrimony.exception.ResourceNotFound;
import com.hcl.matrimony.exception.UnauthorizedUser;
import com.hcl.matrimony.repository.UserRepository;
import com.hcl.matrimony.service.impl.UserServiceImpl;

@ExtendWith(SpringExtension.class)
class UserServiceImplTest {
	@InjectMocks
	private UserServiceImpl userServiceImpl;
	@Mock
	private UserRepository userRepository;

	@Test
	void userRegisteredAlreadyExist() {
		User user = User.builder().email("megha@gmail.com").build();
		Mockito.when(userRepository.findByEmail("megha@gmail.com")).thenReturn(Optional.of(user));
		RegisterDto registerDto = RegisterDto.builder().email("megha@gmail.com").build();

		assertThrows(ResourceConflictExists.class, () -> userServiceImpl.register(registerDto));

	}

	@Test
	void userRegisteredSuccess() {
		User user = User.builder().email("megha@gmail.com").build();
		Mockito.when(userRepository.findByEmail("megha@gmail.com")).thenReturn(Optional.empty());

		Mockito.when(userRepository.save(user)).thenReturn(user);
		RegisterDto registerDto = RegisterDto.builder().email("megha@gmail.com").build();
		ApiResponse apiResponse = userServiceImpl.register(registerDto);

		assertEquals("User Registered sucessfully", apiResponse.getMessage());

	}

	@Test
	void loginSuccess() {
		User user = User.builder().email("megha@gmail.com").password("Megha@123").build();

		Mockito.when(userRepository.findByEmail("megha@gmail.com")).thenReturn(Optional.of(user));

		Mockito.when(userRepository.save(user)).thenReturn(user);

		ApiResponse apiResponse = userServiceImpl.login(user.getEmail(), user.getPassword());
		assertEquals("loggedin sucessfully ", apiResponse.getMessage());

	}

	@Test
	void loginFailure() {

		Mockito.when(userRepository.findByEmail("megha@gmail.com")).thenReturn(Optional.empty());

		assertThrows(ResourceNotFound.class, () -> userServiceImpl.login("megha@gmail.com", "546gfg"));
	}

	@Test
	void invalidUser() {
		User user = User.builder().email("megha@gmail.com").password("Megha@123").build();
		Mockito.when(userRepository.findByEmail("megha@gmail.com")).thenReturn(Optional.of(user));

		assertThrows(UnauthorizedUser.class, () -> userServiceImpl.login("megha@gmail.com", "564egy"));

	}

	@Test
	void logoutSuccess() {
		User user = User.builder().email("megha@gmail.com").password("Megha@123").build();
		Mockito.when(userRepository.findByEmail("megha@gmail.com")).thenReturn(Optional.of(user));
		Mockito.when(userRepository.save(user)).thenReturn(user);
		ApiResponse apiResponse = userServiceImpl.logout(user.getEmail());
		assertEquals("logged out successfully", apiResponse.getMessage());

	}

	@Test
	void logoutFailure() {
		Mockito.when(userRepository.findByEmail("megha@gmail.com")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, () -> userServiceImpl.logout("megh@gmail.com"));

	}

}
